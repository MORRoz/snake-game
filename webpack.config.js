const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

// plugins

//module settings
module.exports = {
    mode: 'development',
    
    context: path.resolve(__dirname, 'src'),

    entry: {
        app: [
            './js/game.js'
        ],        
    },

    plugins: [
        new HtmlWebpackPlugin({
            title: 'Output Management',
            // inlineSource: './src/script-to-inline.js',
            template: './index.html',
            filename: 'index.html',           
            inject: 'body'
        })
    ],

    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist'),
        publicPath : '/dist/'
    }, 

    devServer: {
        contentBase: './dist',
        historyApiFallback: true,
        writeToDisk: true
    },
}

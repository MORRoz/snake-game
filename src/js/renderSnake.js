export default function renderSnake(ctx, arraySnake) {
    ctx.fillStyle = 'green';
    arraySnake.forEach(element => {
        ctx.fillRect(element[0] * 30, element[1] * 30, 28, 28);
    });
}

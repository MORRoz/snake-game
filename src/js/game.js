import renderApple from './renderApple';
import renderSnake from './renderSnake';
import renderScore from './renderScore';


window.onload = startGame();



function startGame() {

    var canvas = document.getElementById('canvas');
    var ctx = canvas.getContext('2d');

    var canvasScore = document.getElementById('scoreBoard');
    var ctxScore = canvasScore.getContext('2d');

    class Snake {
        constructor() {
            this.px = 0;
            this.py = 0;
            this.tail = 2;
            this.arraySnake = [];
            this.arraySnake.push([this.px, this.py]);   
        }

        checkTale() {
            this.arraySnake.push([this.px, this.py]);

            while (this.arraySnake.length > this.tail) {
                this.arraySnake.shift();
            }
        }


        checkCollisions() {
            if (this.px < 0 || this.px > 9 || this.py < 0 || this.py > 9) {
                game.gameOver = 1;
            }

            this.arraySnake.forEach(element => {
                if (element[0] == this.px && element[1] == this.py) {
                    game.gameOver = 1;
                }
            })
        }

    }


    class Apple {
        constructor() {

            this.appleX = Math.floor(Math.random() * Math.floor(10));
            this.appleY = Math.floor(Math.random() * Math.floor(10));
        }
    }

    class Game {
        constructor() {
            this.gameOver = 0;
            this.direction = 'right';
            this.speed = 300;
            this.directionChanged = false;
        }

        gameStep(ctx, game) {

            ctx.clearRect(0, 0, 300, 300);

            if (game.gameOver) {
                ctx.fillStyle = 'red';
                ctx.font = 'bold 48px serif';
                ctx.fillText("Game over", 40, 150);
                ctx.fillStyle = 'red';
                ctx.font = 'bold 36px serif';
                ctx.fillText("Your score: " + snake.tail, 50, 200);
                clearInterval(game.IntervalId);
                 
                document.getElementById('startButton').style.display ='inline-block';
                return;
            }


            if (snake.px == apple.appleX && snake.py == apple.appleY) {
                snake.tail++;
                apple = new Apple();
                game.speed = game.speed - 20;
                clearInterval(game.IntervalId);
                game.IntervalId = setInterval(() => game.gameStep(ctx, game), game.speed);
            }


            if (game.direction == 'right') snake.px++
            else if (game.direction == 'down') snake.py++
            else if (game.direction == 'left') snake.px--
            else if (game.direction == 'up') snake.py--

            game.directionChanged = false;
            
            snake.checkCollisions();
            snake.checkTale();

            renderApple(ctx, apple.appleX, apple.appleY);
            renderSnake(ctx, snake.arraySnake);
            renderScore(ctxScore, game.speed);
        }
    }

    var game = new Game();
    var apple = new Apple();
    var snake = new Snake();

    window.addEventListener('keydown', (e) => keyPressed(e, game));

    document.getElementById('startButton').style.display ='none';

    snake.arraySnake.forEach(element => {
        if (element[0] == apple.appleX && element[1] == apple.appleY) {
            apple = new Apple();
        };
    });

    clearInterval(game.IntervalId);  
    game.IntervalId = setInterval(() => game.gameStep(ctx, game), game.speed);
}

function keyPressed(key, game) {

    if (game.directionChanged) {
        return;
    }
    let lastDirection = game.direction;

    if (key.keyCode == '40' && game.direction != 'up') game.direction = 'down'
    else if (key.keyCode == '38' && game.direction != 'down') game.direction = 'up'
    else if (key.keyCode == '39' && game.direction != 'left') game.direction = 'right'
    else if (key.keyCode == '37' && game.direction != 'right') game.direction = 'left'

    if (lastDirection != game.direction) game.directionChanged = true;
}




export default  function renderScore(ctxScore, speed) {
    ctxScore.clearRect(0, 0, 300, 50);

    ctxScore.fillStyle = 'green';
    ctxScore.font = 'bold 28px serif';
    ctxScore.fillText(`speed: ${speed}`, 20, 30);
}

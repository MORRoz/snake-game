export default function renderApple(ctx, appleX, appleY) {
    ctx.fillStyle = 'yellow';
    ctx.fillRect(appleX * 30, appleY * 30, 28, 28);
}
